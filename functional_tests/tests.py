#from django.test import LiveServerTestCase
import os
from django.contrib.staticfiles.testing import StaticLiveServerTestCase
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
import time
from selenium.common.exceptions import WebDriverException

MAX_WAIT = 10

class NewVisitorTest(StaticLiveServerTestCase):
    

    def setUp(self):
        self.browser = webdriver.Firefox()
        staging_server = os.environ.get('STAGING_SERVER')
        if staging_server:
            self.live_server_url = 'http://' + staging_server

    def tearDown(self):
        self.browser.quit()

    def wait_for_row_in_account_table(self, row_text):

        start_time = time.time()

        while True:
            try:
                table = self.browser.find_element_by_id('account_table')
                rows = table.find_elements_by_tag_name('tr')
                self.assertIn(row_text, [row.text for row in rows])
                return
            except (AssertionError, WebDriverException) as e:
                if time.time() - start_time > MAX_WAIT:
                    raise e
                time.sleep(0.8)

    def test_can_start_an_account_for_one_user(self):
        # Etith has heard about a cool new online money-control  app.
        # She goes to check out its homepage

        self.browser.get(self.live_server_url)

        # She notices the page title and header mention money control

        self.assertIn( 'Accounts', self.browser.title)
        header_text = self.browser.find_element_by_tag_name('h1').text
        self.assertIn('Account', header_text)

        # She is invited to enter a Account straight away
        inputbox = self.browser.find_element_by_id('id_new_account')
        self.assertEqual(
            inputbox.get_attribute('placeholder'),
                'Enter an Account name'
        )

        # She types "Cash" into a text box (Edith wants to control her expenses)
        inputbox.send_keys('Cash')

        # When she hits enter, the page updates, and now the page ask for the amount of money in that account
        inputbox.send_keys(Keys.ENTER)
        self.wait_for_row_in_account_table('1: Cash')
       
        # She likes all her accounts in order
        inputbox = self.browser.find_element_by_id('id_new_account')
        inputbox.send_keys('paysheet')
        inputbox.send_keys(Keys.ENTER)

        # The page updates again and now shows both accounts
        self.wait_for_row_in_account_table('1: Cash')
        self.wait_for_row_in_account_table('2: paysheet')
        
        # There is still a button inviting her to add another account
        # And a button inviting her to add a 'Regist item' (expense or income)

        # She click on that button 
        # The page updates and display a form

        # The form asks:
        # 1) Expense or Income
        # 2) The amount of money transacted
        # 3) A Category of the transaction

        # when she finishes she see the new balance in her account
        #

        # Satisfied she goes back to sleep

    def test_multiple_users_can_start_lists_at_different_urls(self):
        # * Edith starts a new Account 

        self.browser.get(self.live_server_url)
        inputbox = self.browser.find_element_by_id('id_new_account')
        inputbox.send_keys('Cash')
        inputbox.send_keys(Keys.ENTER)
        self.wait_for_row_in_account_table('1: Cash')

        # * She notices that her account has a unique url
        edith_account_url = self.browser.current_url
        self.assertRegex(edith_account_url, '/accounts/.+')

        # * Now a new user, Francisco, comes along to the site,
        
        ''' 
        ** We use a new browser session to make sure that no information 
        ** of Edth's is coming through from cookies etc
        '''
        self.browser.quit()
        self.browser = webdriver.Firefox()

        # * Francis visits the home page. There is no sign of Edith's list

        self.browser.get(self.live_server_url)
        page_text = self.browser.find_element_by_tag_name('body').text
        self.assertNotIn('Cash', page_text)
        self.assertNotIn('paysheet', page_text)


        '''
        ** Francisco starts a new list by entering a new accont.
        ** he is less "REALISTIC" than Edith ...
        '''
        
        inputbox = self.browser.find_element_by_id('id_new_account')
        inputbox.send_keys('Savings')
        inputbox.send_keys(Keys.ENTER)
        self.wait_for_row_in_account_table('1: Savings')

        # * Francisco gets his own unique URL

        francisco_account_url = self.browser.current_url
        self.assertRegex(francisco_account_url, '/accounts/.+' )
        self.assertNotEqual(francisco_account_url, edith_account_url)

        # * Again, ther is no treace of Edith's list
        page_text = self.browser.find_element_by_tag_name('body').text
        self.assertNotIn('Cash', page_text)
        self.assertIn('Savings', page_text)

        # * Satisfied, the both go back to sleep

    def test_layout_and_styling(self):

        # * Edith goes to the home page
        self.browser.get(self.live_server_url)
        self.browser.set_window_size(1024, 768)

        # * She notices the input box is necely centered
        inputbox = self.browser.find_element_by_id('id_new_account')
        self.assertAlmostEqual(
            inputbox.location['x'] + inputbox.size['width'] / 2,
            512,
            delta = 10
        )

        # * She starts a new account and sees the input is nicely centered there too

        inputbox.send_keys('testing')
        inputbox.send_keys(Keys.ENTER)
        self.wait_for_row_in_account_table('1: testing')

        self.browser.set_window_size(1024, 768)
        inputbox = self.browser.find_element_by_id('id_new_account')
        self.assertAlmostEqual(
            inputbox.location['x'] + inputbox.size['width'] / 2,
            512,
            delta = 10
        )
