from django.db import models

# Create your models here.

class Account(models.Model):
    pass

class Transaction(models.Model):
    text = models.TextField(default='')
    account = models.ForeignKey(Account, default=None, on_delete = models.CASCADE )


