from django.urls import resolve
from django.test import TestCase, Client
from django.http import HttpRequest

from accounts.views import home_page
from accounts.models import Transaction, Account 
from django.template.loader import render_to_string

# Create your tests here.


class AccountAndTransactionModelTest(TestCase):

    def test_saving_and_retrieving_transactions(self):

        account_ = Account()
        account_.save()

        first_transaction = Transaction()
        first_transaction.text = 'The first (ever) account transaction'
        first_transaction.account = account_
        first_transaction.save()

        second_transaction = Transaction()
        second_transaction.text = 'Second transaction'
        second_transaction.account = account_
        second_transaction.save()

        saved_account = Account.objects.first()
        self.assertEqual(saved_account, account_)

        saved_transactions = Transaction.objects.all()
        self.assertEqual(saved_transactions.count(), 2)

        first_saved_transaction = saved_transactions[0]
        second_saved_transaction = saved_transactions[1]
        self.assertEqual(first_saved_transaction.text, 'The first (ever) account transaction')
        self.assertEqual(first_saved_transaction.account, account_)
        self.assertEqual(second_saved_transaction.text, 'Second transaction')
        self.assertEqual(second_saved_transaction.account, account_)

class AccountViewTest(TestCase):

    def test_home_page_returns_correct_html(self):
        response = self.client.get('/')
        self.assertTemplateUsed(response, 'home.html')

    def test_passes_correct_account_to_template(self):

        other_account = Account.objects.create()
        correct_account = Account.objects.create()
        response = self.client.get(f'/accounts/{correct_account.id}/')
        self.assertEqual(response.context['account'], correct_account)


    def test_uses_account_template(self):
        account_ = Account.objects.create()
        response = self.client.get(f'/accounts/{account_.id}/')
        self.assertTemplateUsed(response, 'account.html')



    def test_displays_only_transactions_for_that_account(self):

        correct_account = Account.objects.create()

        Transaction.objects.create(text = 'Transaction 1', account = correct_account)
        Transaction.objects.create(text  ='Transaction 2', account = correct_account) 
        other_account = Account.objects.create()
        Transaction.objects.create(text = 'other account Transaction 1', account = other_account)
        Transaction.objects.create(text  ='other account Transaction 2', account = other_account) 


        response = self.client.get(f'/accounts/{correct_account.id}/')

        self.assertContains(response, 'Transaction 1')
        self.assertContains(response, 'Transaction 2')
        self.assertNotContains(response, 'other account Transaction 1')
        self.assertNotContains(response, 'other account Transaction 2')


class NewAccountTest(TestCase):

    def test_can_save_a_POST_request_to_an_existing_account(self):

        other_account = Account.objects.create()
        correct_account = Account.objects.create()

        self.client.post(
                f'/accounts/{correct_account.id}/add_transaction',
                data = {'transaction_text':'A new transaction for an existing account'}
        )

        self.assertEqual(Transaction.objects.count(), 1)
        new_transaction = Transaction.objects.first()                       
        self.assertEqual(new_transaction.text, 'A new transaction for an existing account' )
        self.assertEqual(new_transaction.account, correct_account)



    def test_redirects_to_account_view(self):

        other_account = Account.objects.create()
        correct_account = Account.objects.create()

        response = self.client.post(
                f'/accounts/{correct_account.id}/add_transaction', 
                data = { 'transaction_text':'A new transaction for an existing account' }
        )

        self.assertRedirects(response, f'/accounts/{correct_account.id}/')


