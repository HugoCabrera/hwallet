from django.shortcuts import redirect, render
from django.http import HttpResponse

from accounts.models import Transaction, Account
# Create your views here.

def home_page(request):
    return render(request, 'home.html')

def view_account(request, account_id):
    account_ = Account.objects.get(id = account_id)
    return render(request, 'account.html', {'account': account_})

def new_account(request):

    account_ = Account.objects.create()

    Transaction.objects.create(text = request.POST['transaction_text'], account = account_)
    return redirect('/accounts/' + str(account_.id) + '/')

def add_transaction(request, account_id):

    account_ = Account.objects.get(id = account_id)
    Transaction.objects.create(text = request.POST['transaction_text'], account = account_)
    return redirect('/accounts/' + str(account_.id) + '/')

