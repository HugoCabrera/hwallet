from accounts import views
from django.conf.urls import url

urlpatterns = [
    url(r'^new$', views.new_account, name='new_account'),
    url(r'^(\d+)/$', views.view_account, name='view_account'),
    url(r'^(\d+)/add_transaction$', views.add_transaction, name='add_transaction'),

]
